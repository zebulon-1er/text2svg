# text2svg

Bash script that creates a SVG 1.1 file from given texts and/or text files

I've been inspired by the [David Madison's "Segmented LED Display ASCII Library"](https://www.partsnotincluded.com/segmented-led-display-ascii-library/) project.

# Usage

```
Usage: text2svg.sh [options... ] PATH|TEXT ...
  Build a SVG image from text(s) with a 16-segments LCD design.
  Then create a PNG file from SVG. (if inkscape present)
```

### Options

```
-b, --border                 Set border on. [off]
-c color, --bgcolor=color    Set background color (implies -b). [none]
-h, --help                   Display this help, and exit nicely.
-i, --italic                 Use italic charset. [no]
-n, --no-off                 Dont build unset segments. [no]
-o path, --output=path       Set output file name. [out.svg]
-s num, --scale=num          Set size scale. [1.0]
-t color, --txcolor color    Set text color. [#C0FF20]
```

### Arguments (can have both, and many)
```
PATH    Text file path.
TEXT    Sentence(s).
```

# Prerequis

```
bc - An arbitrary precision calculator language
wc - print newline, word, and byte counts for each file
```
### Optional
```
Inkscape - an SVG (Scalable Vector Graphics) editing program.
```

# Examples

```
$ text2svg.sh -t black 'Hello, world !'
```

will write to default output [out.svg](https://gitlab.com/zebulon-1er/text2svg/-/blob/master/examples/out.svg)

and, [as PNG](https://gitlab.com/zebulon-1er/text2svg/-/blob/master/examples/out.png), if inkscape is present.

```
text2svg.sh -t black -o chars.svg 'Supported chars :' '' chars.txt
```

produces [chars.svg](https://gitlab.com/zebulon-1er/text2svg/-/blob/master/examples/chars.svg)
