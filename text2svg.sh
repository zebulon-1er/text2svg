#!/bin/bash
#
# Script to create a svg (1.1) file from text(s) and/or text file(s) given
# as arguments. See the help for more infos.
#
# Copyright (c)2020-05-10, Frantz "zebulon" aka "zebulon 1er" Balinski.
#
###############################################################################
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
###############################################################################
#
# Prerequis :
#
# bc - An arbitrary precision calculator language
# wc - print newline, word, and byte counts for each file
#
# Optional, only for PNG conversion :
#
# Inkscape - an SVG (Scalable Vector Graphics) editing program.
#
###############################################################################
#
# 2020-05-10 : First release.
#
###############################################################################
#
# BUGS : Since it is a script, double-quote and back-quote chars fail. Don't
#        use them.
#
# TODO : Better dotpoint handling. So it should be set on previous char instead
#        of being a standalone plain single char, each time.
#

PROGNAME="`basename "$0"`"

Error() {
  echo >&2 -e "\e[31m$@\e[39m"
}

Fail() {
  Error 'Error: '"$@"
  exit 1
}

Prerequis() {
  while [ ! -z "$1" ]; do
    which &>/dev/null "$1" || Fail 'requires "'"$1"'"'
    shift
  done
}

Prerequis bc wc

TMPARGS=`getopt -o 'bc:hino:s:t:' -l 'border,bgcolor:,help,italic,no-off,output:,scale:,txcolor:' -n "$PROGNAME" -- "$@"`
test $? -eq 0 || exit 1
eval set -- "$TMPARGS"
unset TMPARGS

Title() {
  if [ ! -z "$1" ]; then
    echo -en '\e[1m'"$1"'\e[21m'
    shift
    while [ ! -z "$1" ]; do
      echo -en " $1"
      shift
    done
    echo
  fi
}

IsANumber() { # int or float
  local S
  S="`echo "$@" | sed 's/[0-9]//g' | sed 's/\.//'`"
  [ -z "$S" ]
}

declare -ir \
  DEFAULT_SCALE=1

declare -ir \
  DEFAULT_BORDER_SIZE=8

declare -r \
  DEFAULT_BORDER='off' \
  DEFAULT_BGCOLOR='none' \
  DEFAULT_FGCOLOR='#C0FF20' \
  DEFAULT_ITALIC='no' \
  DEFAULT_NOOFF='no' \
  DEFAULT_OUTPUT='out.svg'

PrintHelp() {
  echo
  Title 'Usage:' "$PROGNAME" '[options... ] PATH|TEXT ...'
  echo '  Build a SVG image from text(s) with a 16-segments LCD design.'
  echo '  Then create a PNG file from SVG.'
  echo
  Title 'Options:'
  echo '  -b, --border                 Set border on. ['$DEFAULT_BORDER']'
  echo '  -c color, --bgcolor color    Set background color. ['$DEFAULT_BGCOLOR']'
  echo '                                 defining a color implies border on.'
  echo '  -h, --help                   Display this help, and exit nicely.'
  echo '  -i, --italic                 Use italic charset. ['$DEFAULT_ITALIC']'
  echo '  -n, --no-off                 Dont build unset segments. ['$DEFAULT_NOOFF']'
  echo '  -o path, --output=path       Set output file name. ['"$DEFAULT_OUTPUT"']'
  echo '  -s num, --scale=num          Set size scale. ['$DEFAULT_SCALE']'
  echo '                                 scales 16x21 px chars.'
  echo '  -t color, --txcolor color    Set text color. ['$DEFAULT_FGCOLOR']'
  echo
  Title 'Arguments:'
  echo '  PATH    Text file path.'
  echo '  TEXT    Sentence(s).'
  echo
}

BORDER=$DEFAULT_BORDER
BGCOLOR=$DEFAULT_BGCOLOR
FGCOLOR=$DEFAULT_FGCOLOR
ITALIC=$DEFAULT_ITALIC
NOOFF=$DEFAULT_NOOFF
OUTPUT="$DEFAULT_OUTPUT"
SCALE=$DEFAULT_SCALE

while true ; do
  case "$1" in

  -b|--border)
    BORDER='on'
    shift
    continue
    ;;

  -c|--bgcolor)
    BGCOLOR="$2"
    test "$BGCOLOR" != 'none' && BORDER=on
    shift 2
    continue
    ;;

  -h|--help)
    shift
    PrintHelp
    exit 0
    ;;

  -i|--italic)
    shift
    ITALIC='yes'
    continue
    ;;

  -n|--no-off)
    NOOFF='yes'
    shift
    continue
    ;;

  -o|--output)
    OUTPUT="$2"
    shift 2
    continue
    ;;

  -s|--scale)
    IsANumber "$2" || Fail "scale must be a number: '$2'"
    SCALE="`echo "$2" | bc`"
    test "$SCALE" == '0' && Fail "scale must be greater than zero: '$2'"
    shift 2
    continue
    ;;

  -t|--txcolor)
    FGCOLOR="$2"
    shift 2
    continue
    ;;

  --)
    shift
    break
    ;;

  *)
    exit 1
    ;;

  esac
done

test $# -lt 1 && Fail 'required argument(s) PATH or TEXT missing.'

NUMBER_OF_LINES=0
NUMBER_OF_COLUMNS=0

TEMPFILE="`tempfile`" || exit 1
trap "rm -f -- '$TEMPFILE'" EXIT

declare -ir \
  A=$[1<<0] \
  B=$[1<<1] \
  C=$[1<<2] \
  D=$[1<<3] \
  E=$[1<<4] \
  F=$[1<<5] \
  G=$[1<<6] \
  H=$[1<<7] \
  K=$[1<<8] \
  M=$[1<<9] \
  N=$[1<<10] \
  P=$[1<<11] \
  R=$[1<<12] \
  S=$[1<<13] \
  T=$[1<<14] \
  U=$[1<<15] \
  DP=$[1<<16]

Asc() {
  # need locale UTF-8. C only is not enough else chars 128-255 won't get code.
  local LC_CTYPE=C.UTF-8
  printf '%d' "'$1"
}

declare -air SEGMENTS_TABLE=(
  # non displayable chars (0-31)
  # 0:NULL
  # 1:START OF HEADING
  # 2:START OF TEXT
  # 3:END OF TEXT
  # 4:END OF TRANSMISSION
  # 5:ENQUIRY
  # 6:ACKNOWLEDGE
  # 7:BELL
  # 8:BACKSPACE
  # 9:CHARACTER TABULATION
  # 10:LINE FEED (LF)
  # 11:LINE TABULATION
  # 12:FORM FEED (FF)
  # 13:CARRIAGE RETURN (CR)
  # 14:SHIFT OUT
  # 15:SHIFT IN
  # 16:DATA LINK ESCAPE
  # 17:DEVICE CONTROL ONE
  # 18:DEVICE CONTROL TWO
  # 19:DEVICE CONTROL THREE
  # 20:DEVICE CONTROL FOUR
  # 21:NEGATIVE ACKNOWLEDGE
  # 22:SYNCHRONOUS IDLE
  # 23:END OF TRANSMISSION BLOCK
  # 24:CANCEL
  # 25:END OF MEDIUM
  # 26:SUBSTITUTE
  # 27:ESCAPE
  # 28:INFORMATION SEPARATOR FOUR
  # 29:INFORMATION SEPARATOR THREE
  # 30:INFORMATION SEPARATOR TWO
  # 31:INFORMATION SEPARATOR ONE
  # ASCII-7 displayable chars
  [`Asc \ `]=0 # SPACE is just empty
  [`Asc \!`]=$[C|D|DP]
  [`Asc \"`]=$[C|M] # Asc command fails with this special shell char
  [`Asc \#`]=$[C|D|E|F|M|P|S|U]
  [`Asc \$`]=$[A|B|D|E|F|H|M|P|S|U]
  [`Asc \%`]=$[A|D|E|H|M|N|P|S|T|U]
  [`Asc \&`]=$[A|E|F|G|K|M|R|U]
  [`Asc \'`]=$[M]
  [`Asc \(`]=$[N|R]
  [`Asc \)`]=$[K|T]
  [`Asc \*`]=$[K|M|N|P|R|S|T|U]
  [`Asc \+`]=$[M|P|S|U]
  [`Asc \,`]=$[T]
  [`Asc \-`]=$[P|U]
  [`Asc \.`]=$[DP]
  [`Asc \/`]=$[N|T]
  [`Asc \0`]=$[A|B|C|D|E|F|G|H|N|T]
  [`Asc \1`]=$[A|E|F|M|S]
  [`Asc \2`]=$[A|B|E|F|G|N|U]
  [`Asc \3`]=$[A|B|D|E|F|N|P|U]
  [`Asc \4`]=$[C|D|H|P|U]
  [`Asc \5`]=$[A|B|D|E|F|K|P]
  [`Asc \6`]=$[A|D|E|F|G|H|P|U]
  [`Asc \7`]=$[A|B|N|T]
  [`Asc \8`]=$[A|B|D|E|F|G|K|N|P|U]
  [`Asc \9`]=$[A|B|C|D|E|H|P|U]
  [`Asc \:`]=$[M|S]
  [`Asc \;`]=$[M|T]
  [`Asc \<`]=$[N|P]
  [`Asc \=`]=$[A|B|P|U]
  [`Asc \>`]=$[K|U]
  [`Asc \?`]=$[A|B|C|P|S|DP]
  [`Asc \@`]=$[A|B|C|E|F|G|H|M|P]
  [`Asc \A`]=$[C|D|N|P|T]
  [`Asc \B`]=$[A|B|C|D|E|F|M|P|S]
  [`Asc \C`]=$[A|B|E|F|G|H]
  [`Asc \D`]=$[A|B|C|D|E|F|M|S]
  [`Asc \E`]=$[A|B|E|F|G|H|U]
  [`Asc \F`]=$[A|B|G|H|U]
  [`Asc \G`]=$[A|B|D|E|F|G|H|P]
  [`Asc \H`]=$[C|D|G|H|P|U]
  [`Asc \I`]=$[A|B|E|F|M|S]
  [`Asc \J`]=$[C|D|E|F|G]
  [`Asc \K`]=$[G|H|N|R|U]
  [`Asc \L`]=$[E|F|G|H]
  [`Asc \M`]=$[C|D|G|H|K|N]
  [`Asc \N`]=$[C|D|G|H|K|R]
  [`Asc \O`]=$[A|B|C|D|E|F|G|H]
  [`Asc \P`]=$[A|B|C|G|H|P|U]
  [`Asc \Q`]=$[A|B|C|D|E|F|G|H|R]
  [`Asc \R`]=$[A|B|C|G|H|P|U|R]
  [`Asc \S`]=$[A|B|D|E|F|H|P|U]
  [`Asc \T`]=$[A|B|M|S]
  [`Asc \U`]=$[C|D|E|F|G|H]
  [`Asc \V`]=$[G|H|N|T]
  [`Asc \W`]=$[C|D|G|H|R|T]
  [`Asc \X`]=$[K|N|R|T]
  [`Asc \Y`]=$[K|N|S]
  [`Asc \Z`]=$[A|B|E|F|N|T]
  [`Asc \[`]=$[B|E|M|S]
  [`Asc \\`]=$[K|R]
  [`Asc \]`]=$[A|F|M|S]
  [`Asc \^`]=$[C|N]
  [`Asc \_`]=$[E|F]
  [96]=$[K] # ` GRAVE ACCENT -- Asc command fails with this special shell char
  [`Asc \a`]=$[F|S|T|U]
  [`Asc \b`]=$[G|H|T|U]
  [`Asc \c`]=$[F|G|U]
  [`Asc \d`]=$[F|M|S|T]
  [`Asc \e`]=$[F|G|T|U]
  [`Asc \f`]=$[A|G|H|U]
  [`Asc \g`]=$[D|E|P|R]
  [`Asc \h`]=$[G|H|S|U]
  [`Asc \i`]=$[S]
  [`Asc \j`]=$[F|S]
  [`Asc \k`]=$[M|P|R|S]
  [`Asc \l`]=$[E|M|S]
  [`Asc \m`]=$[D|G|P|S|U]
  [`Asc \n`]=$[G|S|U]
  [`Asc \o`]=$[F|G|S|U]
  [`Asc \p`]=$[G|H|K|U]
  [`Asc \q`]=$[A|K|M|S]
  [`Asc \r`]=$[G|U]
  [`Asc \s`]=$[E|P|R]
  [`Asc \t`]=$[F|G|H|U]
  [`Asc \u`]=$[F|G|S]
  [`Asc \v`]=$[G|T]
  [`Asc \w`]=$[D|G|R|T]
  [`Asc \x`]=$[P|R|T|U]
  [`Asc \y`]=$[D|E|R]
  [`Asc \z`]=$[F|T|U]
  [`Asc \{`]=$[B|E|M|S|U]
  [`Asc \|`]=$[M|S]
  [`Asc \}`]=$[A|F|M|P|S]
  [`Asc \~`]=$[C|H|K|P]
  # 127:DELETE (non displayable)
  # ASCII-8 chars
  # non displayable chars (128-159) (== [0..31] + 128)
  # 128:
  # 129:
  # 130:BREAK PERMITTED HERE
  # 131:NO BREAK HERE
  # 132:formerly known as INDEX
  # 133:NEXT LINE (NEL)
  # 134:START OF SELECTED AREA
  # 135:END OF SELECTED AREA
  # 136:CHARACTER TABULATION SET
  # 137:CHARACTER TABULATION WITH JUSTIFICATION
  # 138:LINE TABULATION SET
  # 139:PARTIAL LINE FORWARD
  # 140:PARTIAL LINE BACKWARD
  # 141:REVERSE LINE FEED
  # 142:SINGLE SHIFT TWO
  # 143:SINGLE SHIFT THREE
  # 144:DEVICE CONTROL STRING
  # 145:PRIVATE USE ONE
  # 146:PRIVATE USE TWO
  # 147:SET TRANSMIT STATE
  # 148:CANCEL CHARACTER
  # 149:MESSAGE WAITING
  # 150:START OF GUARDED AREA
  # 151:END OF GUARDED AREA
  # 152:START OF STRING
  # 153:
  # 154:SINGLE CHARACTER INTRODUCER
  # 155:CONTROL SEQUENCE INTRODUCER
  # 156:STRING TERMINATOR
  # 157:OPERATING SYSTEM COMMAND
  # 158:PRIVACY MESSAGE
  # 159:APPLICATION PROGRAM COMMAND
  # ASCII-8 displayable chars
  [`Asc \ `]=0 # 160:NO-BREAK SPACE (commonly abbreviated as NBSP)
  [`Asc \¡`]=$[A|C|D] # 161:INVERTED EXCLAMATION MARK
  [`Asc \¢`]=$[E|F|G|M|P|S|U] # 162:CENT SIGN
  [`Asc \£`]=$[B|E|F|M|P|S|U] # 163:POUND SIGN
  #[`Asc \¤`]=$[] # 164:CURRENCY SIGN (how to draw this char ?)
  [`Asc \¥`]=$[K|N|P|S|U] # 165:YEN SIGN
  [`Asc \¦`]=$[M|S] # 166:BROKEN BAR
  [`Asc \§`]=$[A|D|E|H|K|P|R|U] # 167:SECTION SIGN
  [`Asc \¨`]=$[A|B] # 168:DIAERESIS
  [`Asc \©`]=$[B|E|F|G|H|M|P] # 169:COPYRIGHT SIGN
  [`Asc \ª`]=$[B|C|E|N|P] # 170:FEMININE ORDINAL INDICATOR
  [`Asc \«`]=$[G|H|N|R] # 171:LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
  [`Asc \¬`]=$[D|P|U] # 172:NOT SIGN
  #[173]= non displayable char # 173:SOFT HYPHEN (commonly abbreviated as SHY)
  [174]=$[B|E|F|G|H|M] # 174:REGISTERED SIGN
  [`Asc \¯`]=$[A|B] # 175:MACRON
  [`Asc \°`]=$[A|H|M|U] # 176:DEGREE SIGN
  [`Asc \±`]=$[E|F|M|P|S|U] # 177:PLUS-MINUS SIGN
  [`Asc \²`]=$[B|N|P] # 178:SUPERSCRIPT TWO
  [`Asc \³`]=$[B|C|N|P] # 179:SUPERSCRIPT THREE
  [`Asc \´`]=$[N] # 180:ACUTE ACCENT
  [`Asc \µ`]=$[G|H|M|P|U] # 181:MICRO SIGN
  [`Asc \¶`]=$[A|B|C|D|H|K|M|S|U] # 182:PILCROW SIGN
  [`Asc \·`]=$[U] # 183:MIDDLE DOT
  [`Asc \¸`]=$[E|R] # 184:CEDILLA
  [`Asc \¹`]=$[C] # 185:SUPERSCRIPT ONE
  [`Asc \º`]=$[B|C|E|M|P] # 186:MASCULINE ORDINAL INDICATOR
  [`Asc \»`]=$[C|D|K|T] # 187:RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
  #[`Asc \¼`]=$[] # 188:VULGAR FRACTION ONE QUARTER (can't draw this one)
  #[`Asc \½`]=$[] # 189:VULGAR FRACTION ONE HALF (can't draw this one)
  #[`Asc \¾`]=$[] # 190:VULGAR FRACTION THREE QUARTERS (can't draw this one)
  [`Asc \¿`]=$[E|F|G|M|U|DP] # 191:INVERTED QUESTION MARK
  [`Asc \À`]=$[C|D|N|P|T] # 192:LATIN CAPITAL LETTER A WITH GRAVE
  [`Asc \Á`]=$[C|D|N|P|T] # 193:LATIN CAPITAL LETTER A WITH ACUTE
  [`Asc \Â`]=$[C|D|N|P|T] # 194:LATIN CAPITAL LETTER A WITH CIRCUMFLEX
  [`Asc \Ã`]=$[C|D|N|P|T] # 195:LATIN CAPITAL LETTER A WITH TILDE
  [`Asc \Ä`]=$[C|D|N|P|T] # 196:LATIN CAPITAL LETTER A WITH DIAERESIS
  [`Asc \Å`]=$[C|D|N|P|T] # 197:LATIN CAPITAL LETTER A WITH RING ABOVE
  [`Asc \Æ`]=$[A|B|E|G|H|M|P|S|U] # 198:LATIN CAPITAL LETTER AE
  # Simplified : following caps (C A E I N O U Y) have no accent
  [`Asc \Ç`]=$[A|B|E|F|G|H] # 199:LATIN CAPITAL LETTER C WITH CEDILLA
  [`Asc \È`]=$[A|B|E|F|G|H|U] # 200:LATIN CAPITAL LETTER E WITH GRAVE
  [`Asc \É`]=$[A|B|E|F|G|H|U] # 201:LATIN CAPITAL LETTER E WITH ACUTE
  [`Asc \Ê`]=$[A|B|E|F|G|H|U] # 202:LATIN CAPITAL LETTER E WITH CIRCUMFLEX
  [`Asc \Ë`]=$[A|B|E|F|G|H|U] # 203:LATIN CAPITAL LETTER E WITH DIAERESIS
  [`Asc \Ì`]=$[A|B|E|F|M|S] # 204:LATIN CAPITAL LETTER I WITH GRAVE
  [`Asc \Í`]=$[A|B|E|F|M|S] # 205:LATIN CAPITAL LETTER I WITH ACUTE
  [`Asc \Î`]=$[A|B|E|F|M|S] # 206:LATIN CAPITAL LETTER I WITH CIRCUMFLEX
  [`Asc \Ï`]=$[A|B|E|F|M|S] # 207:LATIN CAPITAL LETTER I WITH DIAERESIS
  [`Asc \Ð`]=$[A|B|C|D|E|F|M|S|U] # 208:LATIN CAPITAL LETTER ETH
  [`Asc \Ñ`]=$[C|D|G|H|K|R] # 209:LATIN CAPITAL LETTER N WITH TILDE
  [`Asc \Ò`]=$[A|B|C|D|E|F|G|H] # 210:LATIN CAPITAL LETTER O WITH GRAVE
  [`Asc \Ó`]=$[A|B|C|D|E|F|G|H] # 211:LATIN CAPITAL LETTER O WITH ACUTE
  [`Asc \Ô`]=$[A|B|C|D|E|F|G|H] # 212:LATIN CAPITAL LETTER O WITH CIRCUMFLEX
  [`Asc \Õ`]=$[A|B|C|D|E|F|G|H] # 213:LATIN CAPITAL LETTER O WITH TILDE
  [`Asc \Ö`]=$[A|B|C|D|E|F|G|H] # 214:LATIN CAPITAL LETTER O WITH DIAERESIS
  [`Asc \×`]=$[K|N|R|T] # 215:MULTIPLICATION SIGN
  [`Asc \Ø`]=$[A|B|C|D|E|F|G|H|N|T] # 216:LATIN CAPITAL LETTER O WITH STROKE
  [`Asc \Ù`]=$[C|D|E|F|G|H] # 217:LATIN CAPITAL LETTER U WITH GRAVE
  [`Asc \Ú`]=$[C|D|E|F|G|H] # 218:LATIN CAPITAL LETTER U WITH ACUTE
  [`Asc \Û`]=$[C|D|E|F|G|H] # 219:LATIN CAPITAL LETTER U WITH CIRCUMFLEX
  [`Asc \Ü`]=$[C|D|E|F|G|H] # 220:LATIN CAPITAL LETTER U WITH DIAERESIS
  [`Asc \Ý`]=$[K|N|S] # 221:LATIN CAPITAL LETTER Y WITH ACUTE
  [`Asc \Þ`]=$[A|G|H|M|U] # 222:LATIN CAPITAL LETTER THORN
  [`Asc \ß`]=$[B|E|M|N|R|S] # 223:LATIN SMALL LETTER SHARP S
  [`Asc \à`]=$[F|K|S|T|U] # 224:LATIN SMALL LETTER A WITH GRAVE
  [`Asc \á`]=$[F|N|S|T|U] # 225:LATIN SMALL LETTER A WITH ACUTE
  [`Asc \â`]=$[C|F|N|S|T|U] # 226:LATIN SMALL LETTER A WITH CIRCUMFLEX
  [`Asc \ã`]=$[C|F|H|K|P|S|T|U] # 227:LATIN SMALL LETTER A WITH TILDE
  [`Asc \ä`]=$[A|B|F|S|T|U] # 228:LATIN SMALL LETTER A WITH DIAERESIS
  [`Asc \å`]=$[B|C|F|M|P|S|T|U] # 229:LATIN SMALL LETTER A WITH RING ABOVE
  [`Asc \æ`]=$[D|E|F|P|R|S|T|U] # 230:LATIN SMALL LETTER AE
  [`Asc \ç`]=$[B|E|M|P|R] # 231:LATIN SMALL LETTER C WITH CEDILLA
  [`Asc \è`]=$[F|G|K|T|U] # 232:LATIN SMALL LETTER E WITH GRAVE
  [`Asc \é`]=$[F|G|N|T|U] # 233:LATIN SMALL LETTER E WITH ACUTE
  [`Asc \ê`]=$[C|F|G|N|T|U] # 234:LATIN SMALL LETTER E WITH CIRCUMFLEX
  [`Asc \ë`]=$[A|B|F|G|T|U] # 235:LATIN SMALL LETTER E WITH DIAERESIS
  [`Asc \ì`]=$[K|S] # 236:LATIN SMALL LETTER I WITH GRAVE
  [`Asc \í`]=$[N|S] # 237:LATIN SMALL LETTER I WITH ACUTE
  [`Asc \î`]=$[C|N|S] # 238:LATIN SMALL LETTER I WITH CIRCUMFLEX
  [`Asc \ï`]=$[A|B|S] # 239:LATIN SMALL LETTER I WITH DIAERESIS
  [`Asc \ð`]=$[A|F|G|K|S|U] # 240:LATIN SMALL LETTER ETH
  [`Asc \ñ`]=$[C|G|H|K|P|S|U] # 241:LATIN SMALL LETTER N WITH TILDE
  [`Asc \ò`]=$[F|G|K|S|U] # 242:LATIN SMALL LETTER O WITH GRAVE
  [`Asc \ó`]=$[F|G|N|S|U] # 243:LATIN SMALL LETTER O WITH ACUTE
  [`Asc \ô`]=$[C|F|G|N|S|U] # 244:LATIN SMALL LETTER O WITH CIRCUMFLEX
  [`Asc \õ`]=$[C|F|G|H|K|P|S|U] # 245:LATIN SMALL LETTER O WITH TILDE
  [`Asc \ö`]=$[A|B|F|G|S|U] # 246:LATIN SMALL LETTER O WITH DIAERESIS
  [`Asc \÷`]=$[A|E|P|U] # 247:DIVISION SIGN
  [`Asc \ø`]=$[F|G|N|S|T|U] # 248:LATIN SMALL LETTER O WITH STROKE
  [`Asc \ù`]=$[F|G|K|S] # 249:LATIN SMALL LETTER U WITH GRAVE
  [`Asc \ú`]=$[F|G|N|S] # 250:LATIN SMALL LETTER U WITH ACUTE
  [`Asc \û`]=$[C|F|G|N|S] # 251:LATIN SMALL LETTER U WITH CIRCUMFLEX
  [`Asc \ü`]=$[A|B|F|G|S] # 252:LATIN SMALL LETTER U WITH DIAERESIS
  [`Asc \ý`]=$[D|E|N|R] # 253:LATIN SMALL LETTER Y WITH ACUTE
  [`Asc \þ`]=$[F|G|H|S|U] # 254:LATIN SMALL LETTER THORN
  [`Asc \ÿ`]=$[A|B|D|E|R] # 255:LATIN SMALL LETTER Y WITH DIAERESIS

  # Filets simples
  [`Asc \─`]=$[P|U] # 0x2500:BOX DRAWINGS LIGHT HORIZONTAL
  [`Asc \│`]=$[M|S] # 0x2502:BOX DRAWINGS LIGHT VERTICAL
  [`Asc \┌`]=$[P|S] # 0x250C:BOX DRAWINGS LIGHT DOWN AND RIGHT
  [`Asc \┐`]=$[S|U] # 0x2510:BOX DRAWINGS LIGHT DOWN AND LEFT
  [`Asc \└`]=$[M|P] # 0x2514:BOX DRAWINGS LIGHT UP AND RIGHT
  [`Asc \┘`]=$[M|U] # 0x2518:BOX DRAWINGS LIGHT UP AND LEFT
  [`Asc \├`]=$[M|P|S] # 0x251C:BOX DRAWINGS LIGHT VERTICAL AND RIGHT
  [`Asc \┤`]=$[M|S|U] # 0x2524:BOX DRAWINGS LIGHT VERTICAL AND LEFT
  [`Asc \┬`]=$[P|S|U] # 0x252C:BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
  [`Asc \┴`]=$[M|P|U] # 0x2534:BOX DRAWINGS LIGHT UP AND HORIZONTAL
  [`Asc \┼`]=$[M|P|S|U] # 0x253C:BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
  [`Asc \╴`]=$[U] # 0x2574:BOX DRAWINGS LIGHT LEFT
  [`Asc \╵`]=$[M] # 0x2575:BOX DRAWINGS LIGHT UP
  [`Asc \╶`]=$[P] # 0x2576:BOX DRAWINGS LIGHT RIGHT
  [`Asc \╷`]=$[S] # 0x2577:BOX DRAWINGS LIGHT DOWN

  # Pavés
  [`Asc \▀`]=$[A|B|C|H|K|M|N|P|U] # 0x2580:UPPER HALF BLOCK
  [`Asc \▁`]=$[E|F] # 0x2581:LOWER ONE EIGHTH BLOCK
  [`Asc \▄`]=$[D|E|F|G|P|R|S|T|U] # 0x2584:LOWER HALF BLOCK
  [`Asc \█`]=$[A|B|C|D|E|F|G|H|K|M|N|P|R|S|T|U] # 0x2588:FULL BLOCK (solid)
  [`Asc \▌`]=$[A|F|G|H|K|M|S|T|U] # 0x258C:LEFT HALF BLOCK
  [`Asc \▏`]=$[G|H] # 0x258F:LEFT ONE EIGHTH BLOCK
  [`Asc \▐`]=$[B|C|D|E|M|N|P|R|S] # 0x2590:RIGHT HALF BLOCK
  [`Asc \▔`]=$[A|B] # 0x2594:UPPER ONE EIGHTH BLOCK
  [`Asc \▕`]=$[C|D] # 0x2595:RIGHT ONE EIGHTH BLOCK
  [`Asc \▖`]=$[F|G|S|T|U] # 0x2596:QUADRANT LOWER LEFT
  [`Asc \▗`]=$[D|E|P|R|S] # 0x2597:QUADRANT LOWER RIGHT
  [`Asc \▘`]=$[A|H|K|M|U] # 0x2598:QUADRANT UPPER LEFT
  [`Asc \▙`]=$[A|D|E|F|G|H|K|M|P|R|S|T|U] # 0x2599:QUADRANT UPPER LEFT AND LOWER LEFT AND LOWER RIGHT
  [`Asc \▚`]=$[A|D|E|H|K|M|P|R|S|U] # 0x259A:QUADRANT UPPER LEFT AND LOWER RIGHT
  [`Asc \▛`]=$[A|B|C|F|G|H|K|M|N|P|S|T|U] # 0x259B:QUADRANT UPPER LEFT AND UPPER RIGHT AND LOWER LEFT
  [`Asc \▜`]=$[A|B|C|D|E|H|K|M|N|P|R|S|U] # 0x259C:QUADRANT UPPER LEFT AND UPPER RIGHT AND LOWER RIGHT
  [`Asc \▝`]=$[B|C|M|N|P] # 0x259D:QUADRANT UPPER RIGHT
  [`Asc \▞`]=$[B|C|F|G|M|N|P|S|T|U] # 0x259E:QUADRANT UPPER RIGHT AND LOWER LEFT
  [`Asc \▟`]=$[B|C|D|E|F|G|M|N|P|R|S|T|U] # 0x259F:QUADRANT UPPER RIGHT AND LOWER LEFT AND LOWER RIGHT
)

echo >"$TEMPFILE" -n 'SENTENCES=('
while [ $# -gt 0 ]; do
  if [ -f "$1" ]; then
    RAWS=`wc -l "$1" | cut -d\  -f1`
    COLS=`wc -L "$1" | cut -d\  -f1`
    test $COLS -gt $NUMBER_OF_COLUMNS && NUMBER_OF_COLUMNS=$COLS
    I=0
    cat "$1" | while [ $I -lt $RAWS ]; do
      IFS='' read STR || exit 1
      echo >>"$TEMPFILE" -n " '$STR'"
      let I++
    done
    let NUMBER_OF_LINES+=$RAWS
    shift
  else
    COLS=${#1}
    test $COLS -gt $NUMBER_OF_COLUMNS && NUMBER_OF_COLUMNS=$COLS
    echo >>"$TEMPFILE" -n " '$1'"
    let NUMBER_OF_LINES++
    shift
  fi
done
echo >>"$TEMPFILE" ' )'

. "$TEMPFILE"

rm -f -- "$TEMPFILE"
trap - EXIT

STROKE_COLOR='#94836F'
FILL_COLOR="$FGCOLOR"
STROKE_WIDTH='0.1'

STYLE_ON="fill:$FILL_COLOR;stroke:none"
STYLE_OFF="fill:none;stroke:$STROKE_COLOR;stroke-width:$STROKE_WIDTH"

LINE_SPACE='5/4'
LINE_HEIGHT="`echo "21*$LINE_SPACE" | bc`"

MAX_WIDTH="`echo "16*$NUMBER_OF_COLUMNS" | bc`"
MAX_HEIGHT="`echo "($LINE_HEIGHT*($NUMBER_OF_LINES-1))+21" | bc`"

BORDER_SIZE=$DEFAULT_BORDER_SIZE

if [ "$BORDER" == 'off' ]; then
  BORDER_LEFT=0
  BORDER_TOP=0
  PAGE_WIDTH=$MAX_WIDTH
  PAGE_HEIGHT=$MAX_HEIGHT
else
  BORDER_LEFT=$BORDER_SIZE
  BORDER_TOP=$BORDER_SIZE
  PAGE_WIDTH="`echo "(2*$BORDER_SIZE)+$MAX_WIDTH" | bc`"
  PAGE_HEIGHT="`echo "(2*$BORDER_SIZE)+$MAX_HEIGHT" | bc`"
fi

VIEW_WIDTH="`echo "$PAGE_WIDTH*$SCALE" | bc`"
VIEW_HEIGHT="`echo "$PAGE_HEIGHT*$SCALE" | bc`"

MARGIN=0
Indent() { let MARGIN+=${1:-1}; }
Unindent() { let MARGIN-=${1:-1}; }
Echo() { printf '%'$[MARGIN*2]'s' ''; echo "$@"; }

if [ "$ITALIC" == 'no' ]; then
  # normal
  SEGMENTS_LIST=( \
    '<path d="M 2.087214,0.52914684 H 6.7458509 V 1.5332213 L 5.6332558,2.6458165 H 2.8334226 L 1.5978375,1.4102313 Z" />' \
    '<path d="M 7.0114679,0.52914687 V 1.5332213 l 1.114662,1.1125952 H 10.924413 L 12.159998,1.4102313 11.670622,0.52914687 Z" />' \
    '<path d="m 12.294357,1.6510438 -1.181842,1.1818415 5.3e-4,7.0342059 1.111561,1.1136288 h 1.004075 V 3.3331139 Z" />' \
    '<path d="m 12.226661,11.24427 -1.113629,1.113629 -5.29e-4,7.033689 1.181842,1.181841 0.934311,-1.68207 V 11.24427 Z" />' \
    '<path d="m 8.1256139,19.579173 -1.114146,1.112079 v 1.004074 h 4.6591541 l 0.489376,-0.881084 -1.235068,-1.235069 z" />' \
    '<path d="m 2.8329058,19.579173 -1.235068,1.235069 0.489376,0.881084 h 4.6586371 v -1.004074 l -1.112078,-1.112079 z" />' \
    '<path d="m 0.52916784,11.24427 v 7.647089 l 0.93482696,1.68207 1.181842,-1.180291 5.29e-4,-7.035756 -1.115179,-1.113112 z" />' \
    '<path d="M 1.4639948,1.6510438 0.52916784,3.3331139 V 10.98072 H 1.5332418 l 1.113112,-1.1131121 -5.17e-4,-7.0362727 z" />' \
    '<path d="M 2.9114538,2.9114336 V 6.150517 l 2.0226191,3.6406096 h 1.465545 l -0.835092,-3.22203 -2.0324371,-3.657663 z" />' \
    '<path d="m 6.8786599,1.7755841 -1.057819,1.0557511 -5.29e-4,3.6716155 0.852146,3.2881759 h 0.412378 L 7.9375109,6.5029507 V 2.8318519 Z" />' \
    '<path d="M 10.22523,2.9114336 8.1938269,6.5675464 7.3577009,9.7911266 h 1.465545 l 2.0236521,-3.641643 v -3.23805 z" />' \
    '<path d="m 7.0114679,10.054677 v 2.115119 h 3.9144951 l 1.058335,-1.056268 -1.058335,-1.058851 z" />' \
    '<path d="m 7.3577009,12.433346 0.836126,3.225131 2.0303691,3.655079 h 0.622702 v -3.23805 l -2.0236521,-3.64216 z" />' \
    '<path d="m 6.6724699,12.433346 -0.852145,3.288176 5.29e-4,3.671616 1.057818,1.057818 1.058851,-1.058335 v -3.669032 l -0.852674,-3.290243 z" />' \
    '<path d="m 4.9351059,12.433346 -2.0236521,3.641127 v 3.239083 h 0.621669 l 2.0303701,-3.655079 0.836125,-3.225131 z" />' \
    '<path d="m 2.8318728,10.054677 -1.058335,1.058851 1.058335,1.056268 h 3.9139781 v -2.115119 z" />' \
    '<ellipse cx="14.419999" cy="20.637508" rx="1.1908141" ry="1.1906265" />' \
  )
else
  # italic
  SEGMENTS_LIST=( \
    '<path d="M 4.6678539,0.52914687 H 9.3264908 L 9.1853774,1.5332213 7.9164173,2.6458164 H 5.1165841 L 4.0546492,1.4102312 Z" />' \
    '<path d="M 9.592108,0.52914687 9.4509945,1.5332213 10.409291,2.6458165 h 2.798284 L 14.61681,1.4102313 14.251262,0.52914687 Z" />' \
    '<path d="m 14.717325,1.6510438 -1.347939,1.1818415 -0.988063,7.0342059 0.95505,1.1136288 h 1.004075 l 1.074801,-7.6476061 z" />' \
    '<path d="m 13.301389,11.24427 -1.27014,1.113629 -0.989049,7.033689 1.015745,1.181841 1.170711,-1.68207 1.074728,-7.647089 z" />' \
    '<path d="M 8.0289475,19.579173 6.758509,20.691252 6.6173956,21.695326 H 11.27655 l 0.613204,-0.881084 -1.06149,-1.235069 z" />' \
    '<path d="m 2.7362394,19.579173 -1.4086456,1.235069 0.3655477,0.881084 H 6.3517786 L 6.492892,20.691252 5.5371065,19.579173 Z" />' \
    '<path d="m 1.6038956,11.24427 -1.07472823,7.647089 0.69842743,1.68207 1.3477211,-1.180291 0.98934,-7.035756 -0.9587413,-1.113112 z" />' \
    '<path d="M 3.8869625,1.6510438 2.715736,3.3331139 1.6409351,10.98072 h 1.004074 L 3.9145588,9.8676079 4.9029254,2.8313352 Z" />' \
    '<path d="M 5.1572853,2.9114336 4.7020618,6.150517 6.2130266,9.7911266 h 1.465545 L 7.2963064,6.5690966 5.7779203,2.9114336 Z" />' \
    '<path d="M 9.2841246,1.7755841 8.0779295,2.8313352 7.5613886,6.5029507 7.9514116,9.7911266 h 0.412378 L 9.6785876,6.5029507 10.194527,2.8318519 Z" />' \
    '<path d="M 12.471062,2.9114336 9.9258253,6.5675464 8.6366546,9.7911266 H 10.1022 l 2.535451,-3.641643 0.455079,-3.23805 z" />' \
    '<path d="M 8.253382,10.054677 7.9561215,12.169796 H 11.870617 L 13.0774,11.113528 12.167877,10.054677 Z" />' \
    '<path d="m 8.2653149,12.433346 0.3828634,3.225131 1.5166817,3.655079 h 0.622702 l 0.455078,-3.23805 -1.5117801,-3.64216 z" />' \
    '<path d="M 7.5800839,12.433346 6.2658159,15.721522 5.750333,19.393138 6.6594844,20.450956 7.8670746,19.392621 8.3827235,15.723589 7.9924629,12.433346 Z" />' \
    '<path d="m 5.8427199,12.433346 -2.5353791,3.641127 -0.4552234,3.239083 h 0.621669 l 2.5440579,-3.655079 1.2893876,-3.225131 z" />' \
    '<path d="m 4.0737869,10.054677 -1.2071467,1.058851 0.9098862,1.056268 H 7.6905045 L 7.987765,10.054677 Z" />' \
    '<ellipse cx="14.419999" cy="20.637508" rx="1.1908141" ry="1.1906265" />' \
  )
fi

# -------------------------------------------------------------------
# Let's go

{
  Echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
  # document
  Echo '<svg'
  Indent
  Echo 'xmlns:dc="http://purl.org/dc/elements/1.1/"'
  Echo 'xmlns:cc="http://creativecommons.org/ns#"'
  Echo 'xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"'
  Echo 'xmlns:svg="http://www.w3.org/2000/svg"'
  Echo 'xmlns="http://www.w3.org/2000/svg"'
  Echo 'version="1.1"'
  Echo 'viewBox="0 0 '$VIEW_WIDTH' '$VIEW_HEIGHT'"'
  Echo 'width="'$VIEW_WIDTH'"'
  Echo 'height="'$VIEW_HEIGHT'">'
  # global scale
  if [ "$SCALE" != '1' ]; then
    Echo '<g transform="scale('$SCALE')">'
    Indent
  fi
  if [ "$BORDER" != 'off' ] && [ "$BGCOLOR" != 'none' ]; then
    # fond
    Echo '<g>'
    Indent
    Echo '<rect'
    Indent
    Echo 'x="0"'
    Echo 'y="0"'
    Echo 'width="'$PAGE_WIDTH'"'
    Echo 'height="'$PAGE_HEIGHT'"'
    Echo 'style="fill:'$BGCOLOR';stroke:none"'
    Unindent
    Echo '/>'
  fi
  for (( Y = 0; Y < NUMBER_OF_LINES; Y++ )); do
    SENTENCE="${SENTENCES[$Y]}"
    for (( X = 0; X < NUMBER_OF_COLUMNS; X++ )); do
      CHAR=${SENTENCE:$X:1}
      ASCII=`Asc "$CHAR"`
      SEGMENTS=${SEGMENTS_TABLE[$ASCII]}
      test -z "$SEGMENTS" && SEGMENTS=${SEGMENTS_TABLE[32]}
      test $SEGMENTS == 0 && SEGMENTS=${SEGMENTS_TABLE[32]}
      SEGMENT_BITS=`echo "obase=2;$SEGMENTS" | bc`
      SEGMENT_BITS=`printf '%017lu' $SEGMENT_BITS`
      SEGMENT_BITS=${SEGMENT_BITS:$[${#SEGMENT_BITS}-17]}
      if [ "$NOOFF" == 'no' ] || [ $SEGMENTS != 0 ]; then
        # déplacement du caractère
        X0="`echo "(16*$X)+$BORDER_LEFT" | bc`"
        Y0="`echo "($LINE_HEIGHT*$Y)+$BORDER_TOP" | bc`"
        if [ "$Y0" != '0' ] || [ "$X0" != '0' ]; then
          Echo '<g transform="translate('$X0','$Y0')">'
          Indent
        fi
        # caractère
        Echo '<svg'
        Indent
        Echo 'viewBox="0 0 16.933357 22.22499"'
        Echo 'width="16"'
        Echo 'height="21">'
        if [ "$NOOFF" == 'no' ] && [ $SEGMENTS != $[(2<<17)-1] ]; then
          # segments off
          Echo '<g style="'"$STYLE_OFF"'">'
          Indent
          for (( I = 0; I < 17; I++ )); do
            test "${SEGMENT_BITS:$[16-I]:1}" != '1' && Echo "${SEGMENTS_LIST[I]}"
          done
          Unindent
          Echo '</g>' # segments off
        fi
        if [ $SEGMENTS != 0 ]; then
          # segments on
          Echo '<g style="'"$STYLE_ON"'">'
          Indent
          for (( I = 0; I < 17; I++ )); do
            test "${SEGMENT_BITS:$[16-I]:1}" == '1' && Echo "${SEGMENTS_LIST[I]}"
          done
          Unindent
          Echo '</g>' # segments on
        fi
        Unindent
        Echo '</svg>' # caractère
        if [ "$Y0" != '0' ] || [ "$X0" != '0' ]; then
          Unindent
          Echo '</g>' # déplacement du caractère
        fi
      fi
    done
  done
  if [ "$BORDER" != 'off' ] && [ "$BGCOLOR" != 'none' ]; then
    Unindent
    Echo '</g>' # fond
  fi
  if [ "$SCALE" != '1' ]; then
    Unindent
    Echo '</g>' # global scale
  fi
  Unindent
  Echo '</svg>' # document
} >"$OUTPUT"

# PNG conversion
which &>/dev/null inkscape && {
  PNG="${OUTPUT/.svg/.png}"
  inkscape &>.log -e "$PNG" "$OUTPUT" && rm -f .log
}
